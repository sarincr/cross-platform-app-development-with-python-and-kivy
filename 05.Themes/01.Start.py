

from kivymd.app import MDApp
from kivymd.uix.screen import Screen
from kivymd.uix.button import MDRectangleFlatButton


class DemoAPP(MDApp):
    
    
    def build(self):
        self.theme_cls.primary_palette = "Green"
        self.theme_cls.primary_hue = "100"
        #self.theme_cls.theme_style = "Dark"
        scrn = Screen()
        btnflt = MDRectangleFlatButton(text="Button", pos_hint={'center_x' : 0.5, 'center_y' : 0.5})
        scrn.add_widget(btnflt)
        return scrn
        



demo = DemoAPP()
demo.run()
